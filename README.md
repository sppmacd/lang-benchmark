# Language speed benchmark
 
## Algorithm
 
Loads pgm(portable gray map) file to array. This operation require
converting string to numbers. I next step allocate output image in rgb24 format and copy
pgm data into it. ANd then  that search light points on image. If pixel exide threshold checks if adjecting pixels
also exide threshold. If this group is big enough pixels are painted red on output image.
At the end the image is exported to raw data or bmp.


## Launguages
- C++
- D
- C#(by cub8)
- Ruby(by cub8)
- Python
- Zig (work in progress)
- Rust (work in progress)
- C(work in progess)
- Lua(planned)
- JS(planned)
## Help wanted in
- Go
- Perl
- PHP
- Elixir
- Erlang
- Java

## Benchmark

Every program is tested on "gwa.pgm" using command hyperfine 

## Bulding

Bash script should be ready soon.

## Results(21.02.2022)
Ryzen 7 2700U Linux 5.13 Manjaro
Warrning! resulst for now are quite inaccurate
| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `C++` | 11.1 ± 4.8 | 5.1 | 20.5 | 1.00 |
| `D` | 31.5 ± 11.3 | 16.4 | 49.8 | 2.84 ± 1.60 |
| `python 3` | 254.4 ± 22.4 | 236.5 | 313.0 | 22.91 ± 10.14 |
| `ruby` | 478.9 ± 24.0 | 449.9 | 516.1 | 43.15 ± 18.84 |




