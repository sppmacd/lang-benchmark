#include "utils.hpp"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_surface.h>
#include <algorithm>
#include <charconv>
#include <cstdlib>
#include <fstream>
#include <ios>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string_view>
#include <vector>
inline auto move_indexes(u64 &ind1, u64 &ind2, std::string_view str,
                         char fchar) {
  ind1 = ind2 + 1;
  ind2 = str.find(fchar, ind1);
}
inline auto get_substr(u64 ind1, u64 ind2, std::string_view str) {
  auto line = str.substr(ind1, ind2 - ind1);
  return line;
}
struct star {
  u2_32 pos;
  std::vector<u2_32> pixels;
};
struct pgm_img {
  u32 w;
  u32 h;
  u16 max;
  u16 *data;
};
inline auto set_pixel(u32 color, u32 x, u32 y, SDL_Surface *surf) {
  u32 *sdl_px = ((u32 *)surf->pixels) + x + y * surf->w;
  *sdl_px = color;
}
u16 *load_pgm(std::string path, u32 &width, u32 &height, u16 &max_val) {
  std::fstream file;
  file.open(path, std::ios::in);
  std::string content;
  file.seekg(0, std::ios::end);
  if (!file.good()) {
    return nullptr;
  }
  u32 size = file.tellg();

  file.seekg(0, std::ios::beg);
  content.resize(size);
  file.read((char *)content.data(), size);
  std::string_view str = content;
  u64 index_0 = 0;
  u64 index_1 = 0;
  move_indexes(index_0, index_1, str, '\n');
  move_indexes(index_0, index_1, str, '\n');
  move_indexes(index_0, index_1, str, ' ');
  u32 w;
  auto substr = get_substr(index_0, index_1, str);
  sscanf(substr.data(), "%u", &w);
  move_indexes(index_0, index_1, str, '\n');
  substr = get_substr(index_0, index_1, str);
  u32 h;
  sscanf(substr.data(), "%u", &h);
  move_indexes(index_0, index_1, str, '\n');
  substr = get_substr(index_0, index_1, str);
  u32 max;
  sscanf(substr.data(), "%u", &max);
  u16 *data = new u16[w * h];
  for (u32 i = 0; i < w * h; i++) {
    move_indexes(index_0, index_1, str, '\n');
    substr = get_substr(index_0, index_1, str);
    int tmp;
    std::from_chars(substr.data(), substr.data() + substr.size(), tmp);
    data[i] = tmp;
  }
  width = w;
  height = h;
  max_val = max;
  return data;
}
SDL_Surface *to_sdl(u16 *data, u32 w, u32 h, u16 max) {
  SDL_Surface *surf =
      SDL_CreateRGBSurfaceWithFormat(0, w, h, 32, SDL_PIXELFORMAT_RGBA32);
  for (u32 y = 0; y < h; y++) {
    for (u32 x = 0; x < w; x++) {
      pixel_data px;
      u32 *sdl_px = ((u32 *)surf->pixels) + x + y * w;
      u8 col = data[x + y * w] / (max / 255);
      px.channels[0] = col;
      px.channels[1] = col;
      px.channels[2] = col;
      px.channels[3] = 255;
      *sdl_px = px.data;
    }
  }
  return surf;
}
void search_star(star &st, u16 thereshold, u2_32 pos, u2_32 data_size,
                 u16 *data) {
  if (pos.x < data_size.x && pos.y < data_size.y) {
    if (data[pos.x + pos.y * data_size.x] >= thereshold) {
      auto ret = std::find(st.pixels.begin(), st.pixels.end(), pos);
      if (ret == st.pixels.end()) {
        st.pixels.push_back(pos);
        search_star(st, thereshold, {pos.x + 1, pos.y}, data_size, data);
        search_star(st, thereshold, {pos.x, pos.y + 1}, data_size, data);
        search_star(st, thereshold, {pos.x - 1, pos.y}, data_size, data);
        search_star(st, thereshold, {pos.x, pos.y - 1}, data_size, data);
      }
    }
  }
}
auto colorpixels(SDL_Surface *surf, std::vector<u2_32> &pos) {
  for (auto &p : pos) {
    set_pixel(0xff0000ff, p.x, p.y, surf);
  }
}
int main() {
  pgm_img img;

  img.data = load_pgm("gwa.pgm", img.w, img.h, img.max);
  SDL_Surface *surf = to_sdl(img.data, img.w, img.h, img.max);
  for (u32 y = 0; y < img.h; y++) {
    for (u32 x = 0; x < img.w; x+=2) {
      auto &luminace = img.data[x + y * img.w];
      if (luminace > 150) {
        star s;
        s.pos = {x, y};
        search_star(s, 150, s.pos, {(u32)img.w, (u32)img.h}, img.data);
        u32 min_size = 6;
        if (luminace > 400) {
          min_size = 2;
        }
        if (s.pixels.size() > min_size) {
          colorpixels(surf, s.pixels);
        }
      }
    }
  }
  SDL_SaveBMP(surf, "test.bmp");

  delete[] img.data;
  SDL_FreeSurface(surf);
  return 0;
}