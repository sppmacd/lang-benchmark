#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct {
  uint32_t size_x;
  uint32_t size_y;
  uint16_t *buffer;
} pgm_img;

pgm_img load_pgm(const char *path) {
  FILE *f = fopen(path, "rb");
  fseek(f, 0, SEEK_END);
  uint32_t size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *filedata = malloc(size);
  fread(filedata, 1, size, f);
  
}